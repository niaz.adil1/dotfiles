#!/bin/sh

source ~/.config/theme_colors/t.colors.sh

echo "" > ~/.config/i3blocks/config
while read line
do
    eval echo "$line" >> ~/.config/i3blocks/config
done < ~/.config/dotfiles/i3blocks/config
