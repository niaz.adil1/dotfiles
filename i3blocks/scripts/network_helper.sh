#!/bin/sh

TOTAL_USAGE_FILE="$HOME/.config/logs/total_usage.txt"
TOTAL_USAGE_TMP_FILE="$HOME/.config/logs/total_usage_tmp.txt"
USAGE_FILE="$HOME/.config/logs/current_usage.txt"

echo "" > $TOTAL_USAGE_TMP_FILE
echo "" > $USAGE_FILE

LINES=$(nmcli device | awk 'END { print NR }')
INTERFACES=$((LINES-1))

OUTPUT=""
INDEX=$((INTERFACES))


formatData() {
    B_SYMBOL="_B"
    KB_SYMBOL="KB"
    MB_SYMBOL="MB"
    GB_SYMBOL="GB"

    VALUE="0b"

    CURR_BYTES=$1
    CURR_K_BYTES=$((CURR_BYTES/1024))
    CURR_M_BYTES=$((CURR_K_BYTES/1024))
    CURR_G_BYTES=$((CURR_M_BYTES/1024))

    BYTES_LIMIT=$((1024))
    K_BYTES_LIMIT=$((BYTES_LIMIT*1024))
    M_BYTES_LIMIT=$((K_BYTES_LIMIT*1024))
    G_BYTES_LIMIT=$((M_BYTES_LIMIT*1024))

    if [[ $CURR_BYTES -le $BYTES_LIMIT ]]; then
        VALUE="$CURR_BYTES$B_SYMBOL"

    elif [[ $CURR_BYTES -gt $BYTES_LIMIT && $CURR_BYTES -le $K_BYTES_LIMIT ]]; then
        VALUE="$CURR_K_BYTES$KB_SYMBOL"

    elif [[ $CURR_BYTES -gt $K_BYTES_LIMIT && $CURR_BYTES -le $M_BYTES_LIMIT ]]; then
        VALUE="$CURR_M_BYTES$MB_SYMBOL"

    elif [[ $CURR_BYTES -gt $M_BYTES_LIMIT ]]; then
        VALUE="$CURR_G_BYTES$GB_SYMBOL"

    fi


    LENGTH=$(expr length "$VALUE")

    SPACES=""
    case $LENGTH in
        1) SPACES="_____" ;;
        2) SPACES="____" ;;
        3) SPACES="___" ;;
        4) SPACES="__" ;;
        5) SPACES="_" ;;
        *) ;;
    esac

    FOMATTED="$SPACES$VALUE"

    echo "$FOMATTED"
}

while [ $INDEX -gt 1 ]
do
    DEVICE=$(nmcli device | awk "NR==$INDEX" | awk '{print $1}')

    DEVICE_LINE=""
    while read -r line
    do
        DEVICE_NAME=$(echo "$line" | awk '{print $1}')
        
        if [[ $DEVICE_NAME = "$DEVICE:" ]]
        then
            DEVICE_LINE="$line"
        fi

    done < /proc/net/dev

    PREVIOUS_RECEIVED=$(awk "/^$DEVICE/" $TOTAL_USAGE_FILE | awk '{print $2}')
    PREVIOUS_TRANSMITTED=$(awk "/^$DEVICE/" $TOTAL_USAGE_FILE | awk '{print $3}')

    echo "$DEVICE_LINE" | awk '{print $2,$10}' | while read CURRENT_RECEIVED CURRENT_TRANSMITTED
    do

        CURR_REC_BYTES=$((CURRENT_RECEIVED-PREVIOUS_RECEIVED))
        CURR_TR_BYTES=$((CURRENT_TRANSMITTED-PREVIOUS_TRANSMITTED))

        REC_VALUE="$(formatData $CURR_REC_BYTES)"
        TR_VALUE="$(formatData $CURR_TR_BYTES)"

        echo "$DEVICE $REC_VALUE $TR_VALUE" >> "$USAGE_FILE"

        echo "$DEVICE $CURRENT_RECEIVED $CURRENT_TRANSMITTED" >> $TOTAL_USAGE_TMP_FILE
    done

    INDEX=$(( $INDEX - 1 ))
done

cp $TOTAL_USAGE_TMP_FILE $TOTAL_USAGE_FILE
