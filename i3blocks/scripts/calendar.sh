#!/bin/sh

WIDTH=200
HEIGHT=200

case "$BLOCK_BUTTON" in
  1|2|3) 

	posX=$(($BLOCK_X - $WIDTH / 2))
	posY=30

	i3-msg -q "exec yad --calendar \
        --width=$WIDTH --height=$HEIGHT \
	    --undecorated --fixed \
	    --close-on-unfocus --no-buttons \
	    --posx=$posX --posy=$posY \
	    > /dev/null"
esac

echo "  $(date "+%Y-%m-%d ")"
