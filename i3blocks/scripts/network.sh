#!/bin/sh

/bin/sh ~/.config/i3blocks/scripts/network_helper.sh

USAGE_FILE="$HOME/.config/logs/current_usage.txt"

LINES=$(nmcli device | awk 'END { print NR }')
INTERFACES=$((LINES-1))

OUTPUT=""
INDEX=$((INTERFACES))
while [ $INDEX -gt 1 ]
do
    CONNECTION_INFO=$(nmcli device | awk "NR==$INDEX" | awk '{print $1,$2,$3,$4}' | while read DEVICE TYPE STATE CONNECTION
    do
        RECEIVED=$(awk "/^$DEVICE/" $USAGE_FILE | awk '{print $2}')
        TRANSMITTED=$(awk "/^$DEVICE/" $USAGE_FILE | awk '{print $3}')
        if [[ $STATE == "connected" ]]; then
            if [[ $TYPE = "ethernet" ]]; then
                CONNECTION_INFO=" "
                TRAFFIC_DATA="  $RECEIVED  $TRANSMITTED"
            elif [[ $TYPE = "wifi" ]]; then
                CONNECTION_INFO=" 直 $CONNECTION"
                TRAFFIC_DATA="  $RECEIVED  $TRANSMITTED"
            fi
        fi

        echo "$CONNECTION_INFO$TRAFFIC_DATA"
    done)

    OUTPUT="$OUTPUT$CONNECTION_INFO"

    INDEX=$(( $INDEX - 1 ))
done

echo "$OUTPUT "
