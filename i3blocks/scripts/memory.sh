#!/bin/sh
#!/bin/sh

formatData() {
    KiB_SYMBOL="KiB"
    MiB_SYMBOL="MiB"
    GiB_SYMBOL="GiB"

    USED_KiB=$1
    USED_MiB=$(awk "BEGIN {print $USED_KiB/1024}")
    USED_GiB=$(awk "BEGIN {print $USED_MiB/1024}")

    K_BYTES_LIMIT=$((1024))
    M_BYTES_LIMIT=$((K_BYTES_LIMIT*1024))
    G_BYTES_LIMIT=$((M_BYTES_LIMIT*1024))

    FORMATTED_USED_KiB=$(printf "%0.2f\n" $USED_KiB)
    FORMATTED_USED_MiB=$(printf "%0.2f\n" $USED_MiB)
    FORMATTED_USED_GiB=$(printf "%0.2f\n" $USED_GiB)

    VALUE="0KiB"

    if [[ $USED_KiB -le $K_BYTES_LIMIT ]]; then
        VALUE="$FORMATTED_USED_KiB$KiB_SYMBOL"

    elif [[ $USED_KiB -gt $K_BYTES_LIMIT && $USED_KiB -le $M_BYTES_LIMIT ]]; then
        VALUE="$FORMATTED_USED_MiB$MiB_SYMBOL"

    elif [[ $USED_KiB -gt $M_BYTES_LIMIT ]]; then
        VALUE="$FORMATTED_USED_GiB$GiB_SYMBOL"

    fi

    LENGTH=$(expr length "$VALUE")

    SPACES=""
    case $LENGTH in
        1) SPACES="_________" ;;
        2) SPACES="________" ;;
        3) SPACES="_______" ;;
        4) SPACES="______" ;;
        5) SPACES="_____" ;;
        6) SPACES="____" ;;
        7) SPACES="___" ;;
        8) SPACES="__" ;;
        9) SPACES="_" ;;
        *) ;;
    esac

    FOMATTED="$SPACES$VALUE"

    echo "$FOMATTED"
}

TOTAL=$(awk 'NR==1{print $2}' /proc/meminfo)

FREE=$(awk 'NR==2{print $2}' /proc/meminfo)

BUFFER=$(awk 'NR==4{print $2}' /proc/meminfo)
CACHED=$(awk 'NR==5{print $2}' /proc/meminfo)
BUFF_CACHE=$((BUFFER+CACHED))

USED=$((TOTAL-FREE-BUFF_CACHE))

DATA=$(formatData $USED)

echo "   $DATA "
