#!/bin/sh

SHUTDOWN=""
RESTART=""
LOCK=""
SLEEP=""
LOGOUT=""

OPTIONS="$SHUTDOWN\n$RESTART\n$LOCK\n$SLEEP\n$LOGOUT"

CHOICE="$(echo -e "$OPTIONS" | rofi -theme power_config.rasi -p "" -dmenu -selected-row 0)"

case $CHOICE in
    $SHUTDOWN)  shutdown now ;;
    $RESTART)   reboot ;;
    $LOCK)      /bin/sh ~/.config/i3/scripts/i3lock.sh;;
    $SLEEP)     /bin/sh ~/.config/i3/scripts/suspend.sh;;
    $LOGOUT)    i3-msg exit;;
    *)  ;;
esac
