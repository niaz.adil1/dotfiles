#!/bin/sh

USAGE=$(mpstat | awk '$13 ~ /[0-9.]+/ { print 100 - $13"%" }')

USAGE="${USAGE::-1}"

FORMATTED_USAGE=$(printf "%0.2f\n" $USAGE)

echo "  $FORMATTED_USAGE% "
