#!/bin/sh


BAT=$(acpi -b | grep -E -o '[0-9]*?%')

IS_POWER_CONNECTED=$(cat /sys/class/power_supply/ADP1/online)

WITHOUT_POWER=(    )
WITH_POWER=(    )

BAT_=${BAT::-1}
ICON_INDEX=$((BAT_/20-1))

[ "$IS_POWER_CONNECTED" = "0" ] && ICON="${WITHOUT_POWER[$ICON_INDEX]}" || ICON=${WITH_POWER[$ICON_INDEX]}

# Full and short texts
echo " $ICON $BAT "

# Set urgent flag below 5% or use orange below 20%
[ ${BAT%?} -le 5 ] && exit 33
[ ${BAT%?} -le 20 ] && echo "#FF8000"

exit 0
