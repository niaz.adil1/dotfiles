#!/bin/sh

TEMP=$(acpi -t | grep -E -o '[0-9][0-9].[0-9]?')

# Full and short texts
echo "  $TEMP°C "

# Set urgent flag below 5% or use orange below 20%
[ ${TEMP%?} -gt 60 ] && exit "#FF8000"
[ ${TEMP%?} -gt 75 ] && echo "#FF0000"

exit 0
