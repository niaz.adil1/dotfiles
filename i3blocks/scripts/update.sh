#!/bin/sh

PACMAN_UPDATES="$(checkupdates | awk 'END{print NR}')"
YAY_UPDATES="$(yay -Qu)"

if [ -z "$YAY_UPDATES" ]
then
  YAY_UPDATES=0
fi

# echo "  $PACMAN_UPDATES - 落 $YAY_UPDATES "
echo "  $PACMAN_UPDATES "
