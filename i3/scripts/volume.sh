#!/bin/sh

MESSAGE_TITLE=""
MESSAGE_BODY=""

if [[ $1 = "up" ]]; then
    pamixer -i 5
    MESSAGE_TITLE="Volume Up"
    MESSAGE_BODY="$NEW_LEVEL"
elif [[ $1 = "down" ]]; then
    pamixer -d 5
    MESSAGE_TITLE="Volume Down"
    MESSAGE_BODY="$NEW_LEVEL"
elif [[ $1 = "mute" ]]; then
    pamixer -t
    MESSAGE_TITLE="Volume (Un)Muted"
    MESSAGE_BODY=""
fi

CURRENT_LEVEL="$(pamixer --get-volume)"

dunstify "$MESSAGE_TITLE" "$CURRENT_LEVEL%" -r 11098
