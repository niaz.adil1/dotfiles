#!/bin/sh

MESSAGE_TITLE=""
MESSAGE_BODY=""

if [[ $1 = "up" ]]; then
    CHANGE="+5%"
    MESSAGE_TITLE="Brightness Up"
    MESSAGE_BODY="$CHANGE"
elif [[ $1 = "down" ]]; then
    CHANGE="5%-"
    MESSAGE_TITLE="Brightness Down"
    MESSAGE_BODY="$CHANGE"
fi

RESPONSE=$(brightnessctl -c backlight set "$CHANGE" | awk 'NR==3{print $4}' | grep -E -o '[0-9]*%?')

dunstify "$MESSAGE_TITLE" "$RESPONSE" -r 11099
