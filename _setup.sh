#!/bin/sh

echo "Deploying dotfiles..."
# LOGS
mkdir -p $HOME/.config/logs
touch $HOME/.config/logs/total_usage.txt
touch $HOME/.config/logs/total_usage_tmp.txt
touch $HOME/.config/logs/current_usage.txt

# THEME
mkdir -p $HOME/.icons/default
cp $HOME/.config/dotfiles/_theme/index.theme $HOME/.icons/default/index.theme

# dialog
cp -R $HOME/.config/dotfiles/dialog/.dialogrc $HOME/.dialogrc

# i3
mkdir -p "$HOME/.config/i3"
cp -R $HOME/.config/dotfiles/i3/. $HOME/.config/i3/

# Polybar
mkdir -p $HOME/.config/polybar
cp -R $HOME/.config/dotfiles/polybar/. $HOME/.config/polybar/

# I3block
mkdir -p $HOME/.config/i3blocks/scripts
/bin/sh $HOME/.config/dotfiles/i3blocks/setup.sh
cp -R $HOME/.config/dotfiles/i3blocks/scripts/. $HOME/.config/i3blocks/scripts/

# Dunst
killall dunst &>/dev/null
mkdir -p $HOME/.config/dunst
/bin/sh $HOME/.config/dotfiles/dunst/setup.sh

# GTK
cp $HOME/.config/dotfiles/gtk/.gtkrc-2.0 $HOME/.gtkrc-2.0

mkdir -p $HOME/.config/gtk-2.0
cp $HOME/.config/dotfiles/gtk/gtk-2.0/gtkfilechooser.ini $HOME/.config/gtk-2.0/gtkfilechooser.ini

mkdir -p $HOME/.config/gtk-3.0
cp $HOME/.config/dotfiles/gtk/gtk-3.0/settings.ini $HOME/.config/gtk-3.0/settings.ini

# RANGER
cp $HOME/.config/dotfiles/ranger/rc.conf "$HOME/.config/ranger/rc.conf"

# ROFI
mkdir -p $HOME/.config/rofi
cp -R $HOME/.config/dotfiles/rofi/. $HOME/.config/rofi/

# ZSH
cp -R $HOME/.config/dotfiles/zsh/. $HOME/

# X
cp $HOME/.config/dotfiles/X/.xinitrc $HOME/.xinitrc

echo "#include \"$HOME/.config/theme_colors/t.Xresources\"" > $HOME/.config/dotfiles/X/.Xresources
cat $HOME/.config/dotfiles/X/.Xresources_ >> $HOME/.config/dotfiles/X/.Xresources
cp $HOME/.config/dotfiles/X/.Xresources $HOME/.Xresources

# Code
mkdir -p "$HOME/.config/Code - OSS/User"
cp $HOME/.config/dotfiles/code/settings.json "$HOME/.config/Code - OSS/User/settings.json"

# VIM
cp $HOME/.config/dotfiles/vim/.vimrc $HOME/.vimrc
vim +'PlugInstall --sync' +qa &>/dev/null

# NEOFETCH
mkdir -p $HOME/.config/neofetch
cp $HOME/.config/dotfiles/neofetch/config $HOME/.config/neofetch/config

echo "Done."
