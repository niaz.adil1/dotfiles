HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

setopt beep
setopt HIST_IGNORE_SPACE
setopt appendhistory
setopt SHARE_HISTORY

source ~/.config/theme_colors/tty.sh

neofetch

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

ZSH_DISABLE_COMPFIX=true

if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH

  autoload -Uz compinit
  compinit
fi

export ZSH_POWER_LEVEL_THEME=/usr/share/zsh-theme-powerlevel10k
source $ZSH_POWER_LEVEL_THEME/powerlevel10k.zsh-theme
#source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"



##############################
#
#	FUNCTIONS
#
##############################
# Secure-shred
sshred() {
  read "choice?Continue secure deletion of files recursively (y/n)?"
  case "$choice" in
    y|Y|"" ) find $1 -depth -type f -exec shred -zvu -n  5 {} \;
    rm -rf $1
    ;;
    n|N ) echo "abort"
    ;;
    * ) echo "invalid";;
  esac
}

# Wallpaper
wallpaper() {
  feh --bg-scale "$1"

  convert "$1" ~/wallpaper.png
}

# Themit
themit() {
  python ~/.config/theme_colors/themit/main.py $1 $2
}

# Update
update() {
  sudo pacman -S archlinux-keyring

  sudo pacman -Syyu

  yay

  sudo freshclam

  cd $HOME/.config/theme_colors/themit && git pull && cd -

  cd $HOME/.config/dotfiles && git pull && cd -

  . $HOME/.config/dotfiles/_setup.sh
}

# Update
net_traffic() {
  iftop -t -s 1 -i $1 | awk 'NR==6' | awk '{print $6}'
}

##############################
#
#	ALIASES
#
##############################

alias clear="clear && neofetch"

alias ls="ls -Glah --color=auto"

alias zsrc="source ~/.zshrc"

# Download youtube
alias yda="youtube-dl --extract-audio --audio-format m4a"

# dotfiles
alias dotfiles=". $HOME/.config/dotfiles/_setup.sh"

# Display
alias dual-display="xrandr --output eDP-1 --auto --output HDMI-1 --auto --left-of eDP-1"
alias single-display="xrandr --output eDP-1 --auto"

# Wifi
alias scan-wifi="nmcli device wifi list"

#git
alias git-tree="git log --oneline --graph --color --all --decorate"

#mpv shuffle
alias mpvs="mpv --shuffle"

# system
alias sysreload="i3-msg reload && i3-msg restart && xrdb ~/.Xresources"

# disk
alias m_disk="udisksctl mount -b $1"
alias um_disk="udisksctl unmount -b $1"

# Setup the alias. Put this in your .bashrc or .zshrc file so it's available at startup.
# alias enhance='function ne() { docker run --rm -v "$(pwd)/`dirname ${@:$#}`":/ne/input -it alexjc/neural-enhance ${@:1:$#-1} "input/`basename ${@:$#}`"; }; ne'

export NVM_DIR="$HOME/.nvm"
  [ -s "/usr/share/nvm/nvm.sh" ] && . "/usr/share/nvm/nvm.sh"  # This loads nvm
  [ -s "/usr/share/nvm/etc/bash_completion.d/nvm" ] && . "/usr/share/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
source /usr/share/nvm/init-nvm.sh

# FZF
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
export FZF_DEFAULT_OPTS='--height=40% --preview="cat {}" --preview-window=right:60%:wrap'

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# ZSH-Z
source /usr/share/zsh/plugins/zsh-z/zsh-z.plugin.zsh

# ZSH-SYNTAX-HIGHLIGHTING
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# ZSH-AUTOSUGGESTIONS
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

export BROWSER=/usr/bin/firefox
