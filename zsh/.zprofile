startGUI() {
	read choice"?Display GUI(y/n)?"
	case "$choice" in
		y|Y|"" ) exec startx;;
		n|N ) echo "NO GUI";;
		* )	echo "INVALID ARGUMENT"
			startGUI;;
	esac
}

GUI_Check() {
	if [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]]; then
		startGUI
	fi
}

GUI_Check
