#!/bin/sh

UPDATES_PACMAN=$(pacman -Qq | wc -l)
UPDATES_YAY=$(yay -Syu | wc -l)

UPDATES=$(("$UPDATES_PACMAN" + "$UPDATES_YAY"))

[ "$UPDATES" -gt 0 ] && echo "$UPDATES_PACMAN-%{F#aa4433}$UPDATES_YAY"
