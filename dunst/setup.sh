#!/bin/sh

replace_characters() {
    LINE=$1
    FUNC_TYPE=$2

    CHARS=(\" \< \> \/ \\)
    ESC_CHARS=(_QTS_ _LT_ _GT_ _SLSH_ _B_SLSH_)

    [[ $FUNC_TYPE = "escape" ]] && ARR_C=( "${CHARS[@]}" ) || ARR_C=( "${ESC_CHARS[@]}" )
    [[ $FUNC_TYPE = "unescape" ]] && ARR_R=( "${CHARS[@]}" ) || ARR_R=( "${ESC_CHARS[@]}" )

    ESCAPED=$LINE
    for i in {0..4};
    do
        ITEM_C=${ARR_C[i]}
        ITEM_R=${ARR_R[i]}


        ESCAPED=${ESCAPED//$ITEM_C/$ITEM_R}
    done

    echo $ESCAPED
}

source ~/.config/theme_colors/t.colors.sh

echo "" > ~/.config/dunst/dunstrc
while read -r line
do

    firstCharacter=${line:0:1}

    if [[ !(-z "$firstCharacter" || "$firstCharacter" == "#") ]]; then

        ESCAPED=$(replace_characters "$line" "escape")
        VARIABLE_UPDTAED=$(eval echo "$ESCAPED")
        UNESCAPED=$(replace_characters "$VARIABLE_UPDTAED" "unescape")

        echo $UNESCAPED >> ~/.config/dunst/dunstrc
    fi
done < ~/.config/dotfiles/dunst/dunstrc
